; -*-Scheme-*-

(define (cont-frac-recur n d k)
  (define (recur i)
    (/ (n i) (+ (d i)
		(if (= i k)
		    0
		    (recur (+ i 1))))))
  (recur 1))


(define (cont-frac-iter n d k)
  (define (iter i result)
    (if (= 0 i)
	result
	(iter (- i 1) (/ (n i)
			 (+ (d i) result)))))
  (iter k 0))


(/ 1 (cont-frac-iter (lambda (i) 1.0)
		     (lambda (i) 1.0)
		     100))

(/ 1 (cont-frac-iter (lambda (i) 1.0)
		     (lambda (i) 1.0)
		     9))
