(define (midpoint-segment segment)
  (let ((p1 (start-segment segment))
	(p2 (end-segment segment)))
    (make-point (* 0.5 (+ (x-point p1) (x-point p2)))
		(* 0.5 (+ (y-point p1) (y-point p2))))))

(define (make-segment p1 p2) (cons p1 p2))
(define (start-segment segment) (car segment))
(define (end-segment segment) (cdr segment))

(define (make-point x y) (cons x y))
(define (x-point point) (car point))
(define (y-point point) (cdr point))

(define (print-point p)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")")
  (newline))


;;; testing

(print-point (midpoint-segment (make-segment (make-point 1 2)
					     (make-point 4 5))))
