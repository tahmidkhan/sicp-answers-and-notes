(define tolerance 0.00001)
(define (close-enough? x y) (< (abs (- x y)) tolerance))

(define (fixed-point f first-guess)
  (define (try guess)
    (define next (f guess))
    (if (close-enough? guess next)
	next
	(try next)))
  (try first-guess))

;;; golden ratio as a fixed-point
(fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.0)
