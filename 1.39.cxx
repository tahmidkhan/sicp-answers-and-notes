#include <iostream>

long double recur(int i, int k, long double x, long double numer)
{
	if (i==k) return 0;
	auto denom = [](int i) -> int { return 1 + 2*i; };
	return numer / (denom(i) - recur(i+1, k, x, x*numer));
}

long double tan_cf(long double x, int k)
{
	return recur(0, k, x, x);
}

int main()
{
	int K;
	std::cin >> K;
	std::cout << tan_cf(3.14159, K) << '\n';
}
