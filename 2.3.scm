;;; dependencie

(define (make-segment p1 p2) (cons p1 p2))
(define (start-segment segment) (car segment))
(define (end-segment segment) (cdr segment))

(define (make-point x y) (cons x y))
(define (x-point point) (car point))
(define (y-point point) (cdr point))


;;; implementation 1

(define (perimeter-rect rect)
  (let ((p1 (start-rect rect))
	(p2 (end-rect rect)))
    (let ((dx (abs (- (x-point p2) (x-point p1))))
	  (dy (abs (- (y-point p2) (y-point p1)))))
      (* 2 (+ dx dy)))))

(define (area-rect rect)
  (let ((p1 (start-rect rect))
	(p2 (end-rect rect)))
    (let ((dx (abs (- (x-point p2) (x-point p1))))
	  (dy (abs (- (y-point p2) (y-point p1)))))
      (* dx dy))))

(define (make-rect p1 p2) (cons p1 p2))
(define (start-rect rect) (car rect))
(define (end-rect rect) (cdr rect))


;;; implementation 2

(define (make-rectangle seg1 seg2)
  (let (()) (cons seg1 seg2)))
(define (rectangle-vertical-seg rect) (car rect))
(define (rectangle-horizontal-seg rect) (cdr rect))
