(define (gcd m n)
  (if (= 0 n)
      m
      (gcd n (remainder m n))))

(define (make-rat n d)
  (let ((normal-n (if (< d 0) (- n) n))
	(normal-d (if (< d 0) (- d) d)))
    (let ((g (gcd normal-n normal-d)))
      (cons (/ normal-n g) (/ normal-d g)))))

(define (numer rat) (car rat))
(define (denom rat) (cdr rat))
(define (print-rat rat)
  (display (numer rat))
  (display "/")
  (display (denom rat))
  (newline))
