(define (even? x) (= 0 (remainder x 2)))
(define (square x) (* x x))

(define (expmod base exp m)
  (define (f r m)
    (if (and (< 1 r) (< r (- m 1))
	     (= 1 (remainder (square r) m)))
	0
	(remainder (square r) m)))
  (cond ((= 0 exp) 1)
	((even? exp) (f (expmod base (/ exp 2) m) m))
	(else (remainder (* base (expmod base (- exp 1) m)) m))))

(define (miller-rabin-test n)
  (define (f a)
    (cond ((= 0 a)
	   true)
	  ((= 1 (expmod a (- n 1) n))
	   (f (- a 1)))
	  (else
	   false)))
  (cond ((< n 2) false)
	((even? n) (= n 2))
	(else (f (quotient n 2)))))
