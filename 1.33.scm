(define (filtered-accumulate combine filter null-value term a next b)
  (define (iter x result)
    (if (> x b)
	result
	(iter (next x) (if (filter x)
			   (combine (term x) result)
			   result))))
  (iter a null-value))


;;; 1.33(a)

(load "ex1.28.scm")

(define (sum-of-squares-of-primes a b)
  (define (sum x y) (+ x y))
  (define (prime? x) (miller-rabin-test x))
  (define (inc x) (+ x 1))
  (define (square x) (* x x))
  (filtered-accumulate sum prime? 0 square a inc b))


;;; 1.33(b)

(define (prod-+ve-ints-coprime-to-n-until-n n)
  (define (product x y) (* x y))
  (define (coprime-to-n? x)
    (define (gcd m n) (if (= 0 n) m (gcd n (remainder m n))))
    (= 1 (gcd x n)))
  (define (inc x) (+ x 1))
  (define (identity x) x)
  (filtered-accumulate product coprime-to-n? 1 identity 2 inc (- n 1)))
