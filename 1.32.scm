(define (accumulate-recur combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
		(accumulate-recur combiner null-value term (next a) next b))))

(define (accumulate-iter combiner null-value term a next b)
  (define (iter x result)
    (if (> x b)
	result
	(iter (next x) (combiner (term x) result))))
  (iter a null-value))
