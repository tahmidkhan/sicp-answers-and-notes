(define (identity x) x)
(define (compose f g) (lambda (x) (f (g x))))

(define (repeated f n)
  (if (= 0 n)
      identity
      (compose f (repeated f (- n 1)))))

((repeated (lambda (x) (* x x)) 2) 5)
