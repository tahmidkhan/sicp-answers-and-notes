(define (tan-cf x k)
  (define (inc x) (+ x 1))
  (define (denom i) (+ 1 (* 2 i)))
  (define (recur i numer)
    (if (= i k)
	0
	(/ numer
	   (- (denom i)
	      (recur (inc i) (* x numer))))))
  (recur 0 x))

(tan-cf (* (/ 3.0 4) 3.14159) 999999)
