(define (even? x) (= 0 (remainder x 2)))
(define (square x) (* x x))

(define (expmod base exp m)
  (cond ((= 0 exp) 1)
	((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
	(else (remainder (* base (expmod base (- exp 1) m)) m))))

(define (fermat-test n)
  (define (f a)
    (cond ((= 0 a)
	   true)
	  ((= a (expmod a n n))
	   (f (- a 1)))
	  (else
	   false)))
  (cond
   ((< n 2) false)
   ((even? n) (= n 2))
   (else (f (- n 1)))))

(fermat-test 561)
(fermat-test 1105)
(fermat-test 1729)
(fermat-test 2465)
(fermat-test 2821)
(fermat-test 6601)
