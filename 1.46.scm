(define (iterative-improve good-enough? improve)
  (define (recur guess)
    (if (good-enough? guess)
        guess
        (recur (improve guess))))
  recur)


(define tolerance 0.0001)
(define (average x y) (/ (+ x y) 2))
(define (square x) (* x x))

(define (sqrt x)
  (define (close-enough? guess)
    (< (abs (- x (square guess))) tolerance))
  (define (average-damp guess)
    (average guess (/ x guess)))
  ((iterative-improve close-enough? average-damp) 1.0))

(define (fixed-point f first-guess)
  (define (close-enough? guess)
    (< (abs (- guess (f guess))) tolerance))
  ((iterative-improve close-enough? f) first-guess))
