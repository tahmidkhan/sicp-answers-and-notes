(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (simpson f a b n)
  (define (h)
    (/ (- b a) n))
  (define (term y)
    (+ (* 2 (f y))
       (* 4 (f (+ y (h))))))
  (define (next y)
    (+ y (* 2 (h))))
  (* (/ (h) 3.0)
     (+ (- (f b) (f a))
	(sum term
	     a
	     next
	     (- b (h))))))

;;; testing
(define (cube x) (* x x x))
;(simpson cube 0 1 100)
;(simpson cube 0 1 1000)
