(define (cont-frac n d k)
  (define (iter i result)
    (if (= 0 i)
	result
	(iter (- i 1) (/ (n i)
			 (+ (d i) result)))))
  (iter k 0))


(+ 2 (cont-frac (lambda (i) 1.0)
		(lambda (i) (if (= 2 (remainder i 3))
				(* (/ (+ i 2) 3) 2.0)
				1.0))
		999))
