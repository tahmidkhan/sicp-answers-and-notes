(define tolerance 0.00001)
(define (close-enough? x y) (< (abs (- x y)) tolerance))

(define (fixed-point-with-print f first-guess)
  (define (try guess)
    (display guess)
    (newline)
    (define next (f guess))
    (if (close-enough? guess next)
	next
	(try next)))
  (try first-guess))

;;; solution to x**x=1000 i.e. x=log(x,1000) as a fixed-point
(fixed-point (lambda (x) (/ (log 1000) (log x))) 2.0)
